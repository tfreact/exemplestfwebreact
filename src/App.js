import './App.css';
import Hello from './Components/Hello/Hello';
import HelloClass from './Components/HelloClass/HelloClass';
import Info from './Components/Info/Info';
import ListCourses from './Components/Courses/ListCourses';
import { Button, Alert } from 'react-bootstrap'
import Formulaire from './Components/Formulaire/Formulaire';
import UpAndDown from './Components/UpAndDown/UpAndDown';

function App() {
  return (
    <div className="App">
       {/* <Info/>
       <Info info="typeok" isImportant={true} nom="Smith">
             <table>
               <tr>
                 <td> <Hello name="A tous" surnom="A demain" qualite="Bonne soirée"/>
      </td>
                </tr>
             </table>
             <h4>Touché</h4>
             <Alert variant="info">Coucou</Alert>
         </Info>
       <Button variant='success'>Hello</Button>
       <Hello name="A tous" surnom="A demain" qualite="Bonne soirée"/>
       <ListCourses/>
       <hr/>
       <h1>Complexe</h1>
       <HelloClass name='Mike' surnom='ReactMan' qualite='Aucune'/>
       <HelloClass/>
       <br/> */}
       {/* <Formulaire/> */}
       <UpAndDown/>
       
    </div>
  );
}

export default App;
