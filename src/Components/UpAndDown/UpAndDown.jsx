//Importation de la librairie react
import React, {Component, Fragment} from 'react'; 
import Enfant from './Enfant';
//Création de notre fonction
class UpAndDown extends Component 
{
        constructor(props)
        {            
            super(props);
           
            this.state=
            {
                leNom:'Person',
                lePrenom:'Mike'

            }

            this.changeNomEtPrenom=this.changeNomEtPrenom.bind(this);
           
        }
          
        changeNomEtPrenom(nom, prenom)
        {
            this.setState(()=>(
                {
                    leNom:nom,
                    lePrenom:prenom
                }));
        }  
    
          render(){

              return(
                  <Fragment>
                    <Enfant ChangeMoi={this.changeNomEtPrenom} Nom={this.state.leNom} Prenom={this.state.lePrenom}/>);
                        <hr/>
                        <h1>PARENT</h1>
                                <h2>{this.state.leNom}</h2>
                                <h2>{this.state.lePrenom}</h2>
                    </Fragment>
                );
           

          }
   
};

//Autoriser l'import ==> Proposer l'export
export default UpAndDown;