//Importation de la librairie react
import React, {Component, Fragment} from 'react'; 
//Création de notre fonction
class Enfant extends Component 
{
    constructor(props)
    {            
        super(props);
        this.state={ Nom:this.props.Nom,Prenom:this.props.Prenom}
        this.handleInput=this.handleInput.bind(this);

        console.log(this.props.ChangeMoi);
    }

    handleInput(event)
    {
        const {name, type, checked, value} = event.target;
        this.setState(()=>{
            return {[name]:type==='checkbox'?checked:value};            
        });

        
        this.props.ChangeMoi(this.state.Nom, this.state.Prenom);
         
    }
      render(){
        return(
            <Fragment>
           <form>
               <input name="Nom" onChange={this.handleInput} value={this.state.Nom}/>
               <input name="Prenom" onChange={this.handleInput} value={this.state.Prenom}/>
                
           </form>
           <h1>Enfant</h1>
           <h1>{this.state.Nom}</h1>
           <h1>{this.state.Prenom}</h1> 
           </Fragment>
        );
      }
   
};

//Autoriser l'import ==> Proposer l'export
export default Enfant;