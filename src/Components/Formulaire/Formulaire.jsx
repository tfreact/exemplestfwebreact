//Importation de la librairie react
import React, {Component, Fragment} from 'react';

//Création de notre fonction
class Formulaire extends Component 
{
        constructor(props)
        {            
            super(props);
            this.state={ Nom:'NOBODY',Prenom:'test', Compris:true, Sexe:'X'}
            this.handleInput=this.handleInput.bind(this);
        }

        handleInput(event)
        {
            const {name, type, checked, value} = event.target;
            this.setState(()=>({
                [name]:type==='checkbox'?checked:value
            }));
             
        }
          render(){
            return(
                <Fragment>
               <form>
                   <input name="Nom" onChange={this.handleInput} value={this.state.Nom}/>
                   <input name="Prenom" onChange={this.handleInput} value={this.state.Prenom}/>
                   <input  name="Compris" type="checkbox"  onChange={this.handleInput} checked={this.state.Compris}  />Compris?<br/>
                   <input type="radio" value="M" name="Sexe" onChange={this.handleInput}/>M<br/>
                   <input type="radio" value="F" name="Sexe" onChange={this.handleInput}/>F<br/>
                   <input type="radio" value="X" name="Sexe" onChange={this.handleInput}/>X<br/>
               </form>
               <h1>{this.state.Nom}</h1>
               <h1>{this.state.Prenom}</h1>
               <h1>{this.state.Sexe}</h1>
               </Fragment>
            );

          }
   
};

//Autoriser l'import ==> Proposer l'export
export default Formulaire;