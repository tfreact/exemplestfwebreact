//Importation de la librairie react
import React, {Component} from 'react';
import MonStyle from  './Hello.module.css';
//Création de notre fonction
class HelloClass extends Component 
{
        constructor(props)
        {            
            super(props);
            //props est readOnly
            // this.props.surnom='The BEst';
            // this.props.nom='Jonas';
            // this.props.qualite='Dijéridoo';
            const surnom = props.surnom===undefined?'The Best':props.surnom;
            this.state = 
            {
               surnom: surnom,
               name: props.name,
               qualite: props.qualite,
               nb:0

            }
            this.changeNom= this.changeNom.bind(this);
        }
          
          changeNom()
          {
            this.setState((state,props)=>
                          (
                              {
                                    Slogan:'Sauve une arbre, mange un castor', 
                                    name:'David Croket',
                                    nb: state.nb+1
                               }
                           ));
          }
    
          render(){
              
            // NE JAMAIS LE FAIRE ICI ==> Appel Récursif INFIN de l'UPDATE Visuel
            //  this.setState({name:'Bernard'}); //Entraine un refresh du visuel
            //   this.state.name='Bernard';
            //   this.render();

             const {name, surnom, qualite, Slogan, nb} = this.state;
            return(
                <section   className={MonStyle.section}>
                    <h1 className={MonStyle.nom}>Bonsoir {name} !!!</h1>
                    <h2>{surnom} - {qualite}</h2>
                    <h3>{Slogan}</h3>
                    <button onClick={this.changeNom}>ChangeMoi ({nb})</button>
                </section>
            );

          }
   
};

//Autoriser l'import ==> Proposer l'export
export default HelloClass;