import React from 'react';
import InfoStyle from './Info.module.css';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core'; 
const Info = function (props)
{
     let classeTitre= InfoStyle.Title;
    if(props.isImportant===1)
    {
        classeTitre = InfoStyle.Importante;
    }

    return (
           <div>
              <p> {props.isImportant}</p>
           <h1 className={classeTitre}>{props.info}</h1>
           <h2>{props.nom}</h2>
           <p className={InfoStyle.nom}>MISTER NOBODY</p>
           <div id="InfoContenu">
               {props.children}
           </div>
           <Button color="primary">Hello DARKNESS</Button>
           
           </div>
         
    );
}

//Valeurs par défaut
Info.defaultProps=
{
  info:"WARNING : info non remplie",
  nom:"WARNING: Nom non rempli"
}

Info.propTypes =
{
    isImportant: PropTypes.bool,
    info: PropTypes.string,
    nom: PropTypes.string
}

export default Info;