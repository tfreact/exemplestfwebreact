//Importation de la librairie react
import React from 'react';
import MonStyle from  './Hello.module.css';
//Création de notre fonction
const Hello = function(props)
{
   const sectionStyle =
   {
       backgroundColor: 'lime',
       color:"salmon"
    }

    return(
        <section style={sectionStyle} className={MonStyle.section}>
            <h1 className={MonStyle.nom}>Bonsoir {props.name} !!!</h1>
            <h2>{props.surnom} - {props.qualite}</h2>
        </section>
    );
};

//Autoriser l'import ==> Proposer l'export
export default Hello;